# overview
 * created a local repo foo
 * added a submodule bar github
 * checked out a specific tag from that repo, putting submodule in detached head state
 * added a submodule baz from github with a local reference
 * created a remote repository to use as origin
 * clonned a copy of foo to another directory from the remote repo
 * clonned a copy of bar from github to its own repo
 * in the clonned copy of foo, used the command
    `git submodule update --init --reference path/to/local/bar bar`
 * did not git submodule init baz
 * committed readme.md


